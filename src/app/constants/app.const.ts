export class AppConst {
	// prod
	public static serverPath = 'https://lv.technobrainbpo.com/api/';
	public static acServerPath = 'https://lv.technobrainbpo.com/api/accounts/';
	public static qaServerPath = 'https://lv.technobrainbpo.com/api/qa/';
	
	// dev
	// public static serverPath = 'http://127.0.0.1:3000/api/';
	// public static acServerPath = 'http://127.0.0.1:3000/api/accounts/';
	// public static qaServerPath = 'http://127.0.0.1:3001/api/qa/';

	public static mediaPath = 'http://127.0.0.1:3000/api/media/';

	// test
	// public static serverPath = 'http://197.211.14.38:8888/api/';
	// public static acServerPath = 'http://197.211.14.38:8888/api/accounts/';
	// public static qaServerPath = 'http://197.211.14.38:8888/api/qa/';

}
