import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { AppConst } from '../../constants/app.const';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*',
  })
};

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  private acServerURL:string = AppConst.acServerPath;

  constructor(private http: HttpClient) { }

  checkParam(param){
    (!param || param == '') ? param = null : param = param;
    return param;
  }

  createUser(user){
    let url = this.acServerURL+'users/';
    return this.http.post(url, user, httpOptions) 
  }

  getUsers(country_id, is_active){
    let url = this.acServerURL+'users/'+this.checkParam(country_id)+'/'+is_active+'/';
    return this.http.get(url, httpOptions);
  }

  getUserDetails(user_id){
    let url = this.acServerURL+'user/'+this.checkParam(user_id)+'/';
    return this.http.get(url, httpOptions); 
  }

  userActiveToggle(active){
    let url = this.acServerURL+'user_active_toggle/';
    return this.http.post(url, active, httpOptions) 
  }

  resetPass(pass){
    let url = this.acServerURL+'resetpass/';
    return this.http.post(url, pass, httpOptions) 
  }

  searchUsername(name: string): Observable<any> {
    let url = this.acServerURL+'search/username/'+name;
    return this.http.get(url)
    .pipe(
      tap((response: any) => {
        return response;
      })
    );
  }

  searchEmail(email: string): Observable<any> {
    let url = this.acServerURL+'search/email/'+email;
    return this.http.get(url, httpOptions)
    .pipe(
      tap((response: any) => {
        return response;
      })
    );
  }

  changeUsertype(user){
    let url = this.acServerURL+'change_usertype/';
    return this.http.post(url, user, httpOptions) 
  }

  updateUser(user){
    let url = this.acServerURL+'update_user_project/';
    return this.http.post(url, user, httpOptions) 
  }
  
  addUserProjects(user){
    let url = this.acServerURL+'add_user_projects/';
    return this.http.post(url, user, httpOptions) 
  }

  deleteUser(user){
    let url = this.acServerURL+'delete_user/';
    return this.http.post(url, user, httpOptions) 
  }

  newProject(proj){
    let url = this.acServerURL+'projects/';
    return this.http.post(url, proj, httpOptions)
  }

  getCountryProject(country_id){
    let url = this.acServerURL+'projects/'+country_id+'/';
    return this.http.get(url, httpOptions)
  }

  searchProject(project: string): Observable<any> {
    let url = this.acServerURL+'search/project/'+project;
    return this.http.get(url, httpOptions)
    .pipe(
      tap((response: any) => {
        return response;
      })
    );
  }

  getProjects(){
    let url = this.acServerURL+'projects/';
    return this.http.get(url, httpOptions); 
  }

  getCountries(){
    let url = this.acServerURL+'countries/';
    return this.http.get(url, httpOptions);
  }

  getUserProjects(user_id){
    let url = this.acServerURL+'user_projects/'+user_id+'/';
    return this.http.get(url, httpOptions); 
  }

  createCountry(country){
    let url = this.acServerURL+'countries/';
    return this.http.post(url, country, httpOptions);
  }
}
