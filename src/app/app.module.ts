import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule, HttpClient, HTTP_INTERCEPTORS } from '@angular/common/http';
import { Http, RequestOptions } from '@angular/http';
import { 
  DatePipe, 
  APP_BASE_HREF,
  HashLocationStrategy, 
  LocationStrategy
} from '@angular/common';

//materials
import { MaterialModule } from './material/material.module';

import { NgIdleKeepaliveModule } from '@ng-idle/keepalive'; // this includes the core NgIdleModule but includes keepalive providers for easy wireup
import { AvatarModule } from 'ng2-avatar';

//Routing
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavigationModule } from './modules/navigation/navigation.module';

import { NewFormComponent } from './modules/qa/new-form/new-form.component';

@NgModule({
  declarations: [
    AppComponent,
    NewFormComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    NavigationModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
  	HttpClientModule,
    MaterialModule,
    NgIdleKeepaliveModule.forRoot(),
    AvatarModule.forRoot(), 
  ],
  providers: [
    DatePipe,
    { provide: LocationStrategy, useClass: HashLocationStrategy },
  ],
  bootstrap: [AppComponent],
  entryComponents: [NewFormComponent],
})
export class AppModule { }
