import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { MaterialModule } from '../../material/material.module';

import { NavComponent } from './nav/nav.component';

@NgModule({
  declarations: [
  	NavComponent, 
  ],
  imports: [
    CommonModule,
		MaterialModule,
    RouterModule,
  ],
  exports: [
  	NavComponent,
  ],
})
export class NavigationModule { }
