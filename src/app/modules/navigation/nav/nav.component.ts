import { Component, HostBinding, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MatTreeFlatDataSource, MatTreeFlattener } from '@angular/material/tree';
import { NavService } from '../../../services/nav/nav.service';
import { animate, state, style, transition, trigger } from '@angular/animations';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css'],
  animations: [
    trigger('indicatorRotate', [
      state('collapsed', style({transform: 'rotate(0deg)'})),
      state('expanded', style({transform: 'rotate(180deg)'})),
      transition('expanded <=> collapsed',
        animate('225ms cubic-bezier(0.4,0.0,0.2,1)')
      ),
    ])
  ]
})
export class NavComponent implements OnInit {

  sideNavOpened = true;

  menu: any[];

  expanded: boolean;
  @HostBinding('attr.aria-expanded') ariaExpanded = this.expanded;
  @Input() item: NavItem;
  @Input() depth: number;

  constructor(
    public router: Router,
    public navService: NavService,
  ) {
    if (this.depth === undefined) {
      this.depth = 0;
    }
  }

  ngOnInit() {
    this.navService.currentUrl.subscribe((url: string) => {
      // console.log(this.item);
      if (this.item.route && url) {
        // console.log(`Checking '/${this.item.route}' against '${url}'`);
        this.expanded = url.indexOf(`/${this.item.route}`) === 0;
        this.ariaExpanded = this.expanded;
        // console.log(`${this.item.route} is expanded: ${this.expanded}`);
      }
    });

    if (sessionStorage.getItem('user_type') != '2'){
      this.menu = [
        // {
        //   path: '/home/updates',
        //   icon: 'home',
        //   name: 'Updates'
        // },
        // {
        //   path: '/home/huddle',
        //   icon: 'explore',
        //   name: 'Huddles'
        // },
        // {
        //   path: '/home/clarification',
        //   icon: 'send',
        //   name: 'Clarifications'
        // },
        {
          path: '/auth/users',
          icon: 'people',
          name: 'Users'
        },
      ];
    }else{
      this.menu = [
        // {
        //   path: '/home/updates',
        //   icon: 'home',
        //   name: 'Updates'
        // },
        // {
        //   path: '/home/huddle',
        //   icon: 'explore',
        //   name: 'Huddles'
        // },
        // {
        //   path: '/home/clarification',
        //   icon: 'home',
        //   name: 'Clarifications'
        // },
      ];
    }
  }

  onItemSelected(item: NavItem) {
    if (!item.children || !item.children.length) {
      this.router.navigate([item.route]);
    }
    if (item.children && item.children.length) {
      this.expanded = !this.expanded;
    }
  }

  logout(){
    // location.reload();
    this.router.navigate(['/auth/login']);
  }

  isActive(item) {
    return this.router.url == item.path;
  }

  toggleSideNav() {
    this.sideNavOpened = !this.sideNavOpened;
  }
}
export interface NavItem {
  displayName: string;
  disabled?: boolean;
  iconName: string;
  route?: string;
  children?: NavItem[];
}
