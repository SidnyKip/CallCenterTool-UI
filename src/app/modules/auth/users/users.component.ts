import { Component, OnInit, ViewChild, Inject, ViewChildren, QueryList } from '@angular/core';
import { MatTableDataSource, MatPaginator } from '@angular/material';
import { UsersService } from '../../../services/users/users.service';
import { DataService } from '../../../services/data/data.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Subject, Observable, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap, catchError, tap, finalize } from 'rxjs/operators/';
import { AppComponent } from '../../../app.component'; 
import { ActiveComponent } from '../active/active.component'; 

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  change_user: ChangeUser = new ChangeUser();
  users: any;
  inactive_users: any;

  displayedColumns = [
    'name',
    'user_type',
    'email',
    'active',
    'view',
  ];

  dataSourceActive: any;
  dataSourceInactive: any;
  @ViewChildren(MatPaginator) paginator = new QueryList<MatPaginator>();

  applyFilterActive(filterValue: string) {
    this.dataSourceActive.filter = filterValue.trim().toLowerCase();
  }

  applyFilterInactive(filterValue: string) {
    this.dataSourceInactive.filter = filterValue.trim().toLowerCase();
  }

  constructor(
    public _userService: UsersService,
    public dialog: MatDialog,
    public appcomp: AppComponent,
  ) {
    // this.appcomp.checkLogin();
  }

  ngOnInit() {
    this.fetchUsers();
  }

  fetchUsers(){
    this._userService.getUsers(sessionStorage.getItem('country_id'), true).subscribe(data=>{
      this.users = data;
      this.dataSourceActive = new MatTableDataSource(this.users);
      this.dataSourceActive.paginator = this.paginator.toArray()[0];
    });
  }

  fetchInactiveUsers(){
    this._userService.getUsers(sessionStorage.getItem('country_id'), false).subscribe(data=>{
      this.inactive_users = data;
      this.dataSourceInactive = new MatTableDataSource(this.inactive_users);
      this.dataSourceInactive.paginator = this.paginator.toArray()[1];
    });
  }

  tabSwitch(state){
    this.fetchUsers() ? state == 'Active' : this.fetchInactiveUsers();
  }

  openUserDialog(): void {
    let dialogRef = this.dialog.open(NewUserDialog, {
      width: '800px',
      data: { users_list: this.users }
    });
    dialogRef.afterClosed().subscribe(result => {
      this.fetchUsers();
    });
  }

  openUpdateUserDialog(user): void {
    let dialogRef = this.dialog.open(UpdateUserDialog, {
      width: '800px',
      data: { user: user }
    });
    dialogRef.afterClosed().subscribe(result => {
      this.fetchUsers();
    });
  }

  openActivaUserDialog(user, state): void {
    let dialogRef = this.dialog.open(ActiveComponent, {
      width: '800px',
      data: { user: user, state: state }
    });
    dialogRef.afterClosed().subscribe(result => {
      state == 'Activate' ? this.fetchInactiveUsers() : this.fetchUsers();
    });
  }
}

export class ChangeUser{
  id: any;
  user_type: any;
}

@Component({
  selector: 'new_user',
  templateUrl: 'modals/new_user.html',
})
export class NewUserDialog implements OnInit{

  new_user: User = new User();
  users_list: any;
  gc_list = [];
  qc_list = [];
  tc_list = [];
  admin_list: any =[];
  usernames: any;
  usermails: any;

  searchTerms = new Subject<string>();
  userForm: FormGroup;
  emailForm: FormGroup;
  isLoading = false;
  loading = true;
  email_user: emailUser = new emailUser();

  email_exist: boolean = false;
  email_not_exist: boolean = false;
  username_exist: boolean = false;
  username_not_exist: boolean = false;
  savinguser: boolean = false;
  error: any;

  projects: any;
  countries: any;

  email = new FormControl('', [Validators.required, Validators.email]);

  userType = [
    {value: 1, viewValue: 'Agent'},
    {value: 2, viewValue: 'Qc'},
    {value: 3, viewValue: 'Gc'},
    {value: 4, viewValue: 'Tc'},
    {value: 5, viewValue: 'Admin'},
  ];

  constructor(
    private _usersService: UsersService,
    private _dataService: DataService,
    private fb: FormBuilder,
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<NewUserDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit(){
    this.userForm = this.fb.group({
      username: null
    })
    this.userForm
    .get('username')
    .valueChanges
    .pipe(
      debounceTime(300),
      tap(() => this.isLoading = true),
      switchMap(value => this._usersService.searchUsername(value)
      .pipe(
        finalize(() => this.isLoading = false),
        )))
    .subscribe(users =>{
      this.usernames = users;
      if(this.usernames == true){
        this.username_not_exist = true;
        this.username_exist = false;
      }else{
        this.username_exist = true;
        this.username_not_exist = false;
      }
    });

    this.emailForm = this.fb.group({
      email: null
    })
    this.emailForm
    .get('email')
    .valueChanges
    .pipe(
      debounceTime(300),
      tap(() => this.isLoading = true),
      switchMap(value => this._usersService.searchEmail(value)
      .pipe(
        finalize(() => this.isLoading = false),
        )))
    .subscribe(emails => {
      this.usermails = emails
      if(this.usermails == true){
        this.email_not_exist = true;
        this.email_exist = false;
      }else{
        this.email_exist = true;
        this.email_not_exist = false;
      }
    });
    this.onkeyup();
    this.fetchCountries();
  }

  fetchCountryProjects(){
    this.users_list = [];
    this.projects = [];
    this.tc_list = [];
    this.gc_list = [];
    this.qc_list = [];
    this._usersService.getCountryProject(this.new_user['country']).subscribe(data=>{
      this.projects = data;
    })
  }

  fetchProjectUsers(project_id){
    this.users_list = [];
    this.tc_list = [];
    this.gc_list = [];
    this.qc_list = [];
    this._dataService.getProjectUsers(project_id).subscribe(data=>{
      this.users_list = data;
      this.users_list.filter(item =>{
        if(item.role_id == 4){
          this.tc_list.push(item);
        }
        if(item.role_id == 3){
          this.gc_list.push(item);
        }
        if(item.role_id == 2){
          this.qc_list.push(item);
        }
      })
    })
  }

  fetchCountries(){
    this._usersService.getCountries().subscribe(data=>{
      this.countries = data;
    })
  }
  
  onkeyup(){
    this.error = ''
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  addUser(){
    this.savinguser = true;
    this.new_user['email'] = this.emailForm.get('email').value;
    this.new_user['username'] = this.userForm.get('username').value;
    this._usersService.createUser(this.new_user).subscribe(data=>{
      this.new_user = new User();
      this.savinguser = false;
      this.dialogRef.close();
    }, error =>{
      this.savinguser = false;
      this.error = 'Error saving'
    })
  }

  openNewProjectDialog(): void {
    let dialogRef = this.dialog.open(NewProjectDialog, {
      width: '800px',
      data: this.new_user['country']
    });
    dialogRef.afterClosed().subscribe(result => {
      this.fetchCountryProjects();
    });
  }
}

export class User{
  first_name: any;
  last_name: any;
  username: any;
  password: any;
  email: any;
  user_type: any;
  country: any;
  project: any;
  sap: any;
}

export class emailUser{
  email: any;
}

@Component({
  selector: 'update_user',
  templateUrl: 'modals/update_user.html',
})
export class UpdateUserDialog implements OnInit{

  update_user: User = new User();
  projects: any;
  countries: any;

  constructor(
    private _usersService: UsersService,
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<UpdateUserDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) { }
  
  ngOnInit(){
    this.update_user = this.data.user;
    this.update_user['user_id'] = this.data.user.id;
    this.fetchCountryProjects();
    this.fetchCountries();
  }

  fetchCountryProjects(){
    this.projects = [];
    this._usersService.getCountryProject(this.update_user['country']).subscribe(data=>{
      this.projects = data;
    })
  }

  fetchCountries(){
    this._usersService.getCountries().subscribe(data=>{
      this.countries = data;
    })
  }
  
  onNoClick(): void {
    this.dialogRef.close();
  }

  updateUser(){
    this._usersService.updateUser(this.update_user).subscribe(data=>{
      this.dialogRef.close();
    })
  }

  openNewProjectDialog(): void {
    let dialogRef = this.dialog.open(NewProjectDialog, {
      width: '800px',
      data: this.update_user['country']
    });
    dialogRef.afterClosed().subscribe(result => {
      this.fetchCountryProjects();
    });
  }
}
@Component({
  selector: 'new_project',
  templateUrl: 'modals/new_project.html',
})
export class NewProjectDialog implements OnInit{

  new_proj: Project = new Project();
  name_exist: any;
  name_not_exist: any;
  projectnames: any;
  isLoading = false;
  country_id: any;
  nameForm: FormGroup;

  constructor(
    private _usersService: UsersService,
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<NewProjectDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) { }
  

  ngOnInit(){
    this.country_id = this.data;
    this.nameForm = this.fb.group({
      name: null
    })
    this.nameForm
    .get('name')
    .valueChanges
    .pipe(
      debounceTime(300),
      tap(() => this.isLoading = true),
      switchMap(value => this._usersService.searchProject(value)
      .pipe(
        finalize(() => this.isLoading = false),
        )))
    .subscribe(names => {
      this.projectnames = names
      if(this.projectnames == true){
        this.name_not_exist = true;
        this.name_exist = false;
      }else{
        this.name_exist = true;
        this.name_not_exist = false;
      }
    });
    this.onkeyup();
  }
  onkeyup(){
    // this.error = ''
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  addProject(){
    this.new_proj['name'] = this.nameForm.get('name').value;
    this.new_proj['country_id'] = this.country_id;
    this._usersService.newProject(this.new_proj).subscribe(data=>{
      this.dialogRef.close();
    })
  }
}

export class Project{
  name: any;
  country_id: any;
}

