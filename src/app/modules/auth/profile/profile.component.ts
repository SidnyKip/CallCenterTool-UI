import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { AppComponent } from '../../../app.component';
import { DataService } from '../../../services/data/data.service';
import { UsersService } from '../../../services/users/users.service';
import { MatTableDataSource, MatPaginator } from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ActiveComponent } from '../active/active.component'; 
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  id: any;
	projects: any;
	countries: any;
	details: any;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  displayedColumns: string[] = [
																'question',
																'type',
																'weight',
																'answers',
																'delete',
  														];

  constructor(
    private _dataService: DataService,
    private _usersService: UsersService,
    public appcomp: AppComponent,
    private route: ActivatedRoute,
    public dialog: MatDialog,
  ) {
    this.appcomp.reset();
    this.appcomp.checkLogin();
  }

  ngOnInit() {
    this.route.params.subscribe(params=>{this.id = params['id']});
    this.fetchUserDetails();
  }

  fetchUserDetails(){
    this._usersService.getUserDetails(this.id).subscribe(data=>{
      this.details = data;
	    this.fetchCountryProjects();
    })
  }

  fetchCountryProjects(){
    this.projects = [];
    this._usersService.getCountryProject(this.details.country_id).subscribe(data=>{
      this.projects = data;
    })
  }

  openActivaUserDialog(user, state): void {
    let dialogRef = this.dialog.open(ActiveComponent, {
      width: '800px',
      data: { user: user, state: state }
    });
    dialogRef.afterClosed().subscribe(result => {
      this.fetchUserDetails();
    });
  }

  openAddUserProjectDialog(user): void {
    let dialogRef = this.dialog.open(AddUserProjectDialog, {
      width: '800px',
      data: { user: this.details }
    });
    dialogRef.afterClosed().subscribe(result => {
      this.fetchUserDetails();
    });
  }
}

@Component({
  selector: 'add_user_project',
  templateUrl: 'modals/add_user_project.html',
})
export class AddUserProjectDialog implements OnInit{

  update_user: User = new User();
  projects: any;
  admin_projects = new FormControl();
  current_projects: any;

  constructor(
    private _usersService: UsersService,
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<AddUserProjectDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) { }
  
  ngOnInit(){
    this.update_user = this.data.user;
    this.update_user['user_id'] = this.data.user.id;
    this.fetchCountryProjects();
    this.current_projects = this.update_user.projects;
  }

  fetchCountryProjects(){
    // console.log(this.update_user);
    this.projects = [];
    this._usersService.getCountryProject(this.update_user.country_id).subscribe(data=>{
      this.projects = data;

      this.current_projects.forEach(item =>{
        // console.log(item.project_id);
        this.projects.forEach(proj => {
          // console.log(proj.id);
          if(item.project_id == proj.id)
            proj['disabled'] = true;
        })
      });

      console.log(this.projects);
    })
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  addUserProjects(){
    this.update_user['project'] = this.admin_projects.value;
    var obj = {
      'user_id': this.update_user['user_id'],
      'projects': this.update_user['project']
    }
    this._usersService.addUserProjects(obj).subscribe(data=>{
      this.dialogRef.close();
    })
  }
}

export class User{
  first_name: any;
  last_name: any;
  username: any;
  password: any;
  user_type: any;
  email: any;
  project: any;
  user: any;
  country_id: any;
  projects: any;
}