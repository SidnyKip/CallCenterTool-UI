import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

//materials
import { MaterialModule } from '../../material/material.module';

//routing
import { AuthRoutingModule } from './auth-routing.module';

//services
import { LoginService } from '../../services/login/login.service';
import { UsersService } from '../../services/users/users.service';

import { NavigationModule } from '../navigation/navigation.module';
//components
import { LoginComponent } from './login/login.component';
import { SettingsComponent } from './settings/settings.component';
import { 
  UsersComponent,
  NewUserDialog,
  UpdateUserDialog,
  NewProjectDialog,
 } from './users/users.component';
import { 
  ProfileComponent,
  AddUserProjectDialog
} from './profile/profile.component';
import { ActiveComponent } from './active/active.component';

@NgModule({
  declarations: [
    LoginComponent, 
    SettingsComponent, 
    UsersComponent,
    NewUserDialog,
    UpdateUserDialog,
    NewProjectDialog,
    ProfileComponent,
    AddUserProjectDialog,
    ActiveComponent,
  ],
  imports: [
    CommonModule,
    AuthRoutingModule,
    NavigationModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
  ],
  providers: [
  	LoginService,
    UsersService,
  ],
  entryComponents: [
    NewUserDialog,
    UpdateUserDialog,
    NewProjectDialog,
    ActiveComponent,
    AddUserProjectDialog,
  ],
})
export class AuthModule { }
