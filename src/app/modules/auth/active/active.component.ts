import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { UsersService } from '../../../services/users/users.service';

@Component({
  selector: 'app-active',
  templateUrl: './active.component.html',
  styleUrls: ['./active.component.css']
})
export class ActiveComponent implements OnInit {

	activate: boolean;
	deactivate: boolean;
  active_user: ActivateUser = new ActivateUser();
  user: any;

  constructor(
    private _usersService: UsersService,
    public dialogRef: MatDialogRef<ActiveComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) { }
  
  ngOnInit() {
    this.user = this.data.user;
    this.active_user['user_id'] = this.user.id;
    this.data.state == 'Activate' ? this.activate = true : this.deactivate = true;
    this.data.state == 'Activate' ? this.active_user['active'] = true : this.active_user['active'] = false;
  }

  userActiveToggle(){
    this._usersService.userActiveToggle(this.active_user).subscribe(data=>{
      this.dialogRef.close();
    })
  }
}

export class ActivateUser{ }
