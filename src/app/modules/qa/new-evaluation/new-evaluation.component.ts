import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { MatTableDataSource, MatPaginator } from '@angular/material';
import { DataService } from '../../../services/data/data.service';
import { UsersService } from '../../../services/users/users.service';
import { CommonModelService } from '../../../services/common-model/common-model.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatDialogModule } from '@angular/material';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Subject, Observable, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap, catchError, tap, finalize } from 'rxjs/operators/';
import { AppComponent } from '../../../app.component'; 
import { Router } from "@angular/router";
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-new-evaluation',
  templateUrl: './new-evaluation.component.html',
  styleUrls: ['./new-evaluation.component.css']
})
export class NewEvaluationComponent implements OnInit {
  evaluations: any;
  user_projects: any;
  forms_list: any = [];
  evaluations_list: any = [];
  displayedColumns: string[];
  dataSource: any;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  eva_form: EvaluationForm = new EvaluationForm(); 
  message:string;

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  constructor(
    public _dataService: DataService,
    public _userService: UsersService,
    private commModel: CommonModelService,
    private router: Router,
    public dialog: MatDialog,
    public appcomp: AppComponent,
    public datepipe: DatePipe,
  ) {
    this.appcomp.checkLogin();
  }

  ngOnInit() {
    this.fetchUserProjects();
    this._dataService.currentMessage.subscribe(message => this.message = message)
  }

  navigateTo(){
    this.eva_form['call_date'] = this.datepipe.transform(this.eva_form['call_date'], 'yyyy-MM-dd');
    this._dataService.changeMessage(this.eva_form);
    this.router.navigate(['/qa/evaluate/']);
  }

  fetchUserProjects(){
    this._userService.getUserProjects(sessionStorage.getItem('user_id')).subscribe(data=>{
      this.user_projects = data;
    });
  }

  fetchProjectForms(project_id){
    this.forms_list = [];
    this._dataService.getProjectForms(project_id, sessionStorage.getItem('user_id')).subscribe(data=>{
      this.forms_list = data;
      // Only show forms that have categories
      this.forms_list.forms = this.forms_list.forms.filter(form => form.total > 0);
    });
  }

  openNewFormDialog(): void {
    this.commModel.openDialog().subscribe(data =>{
      if (data != undefined){
        this.router.navigate(['/qa/new-category/'+data]);
      }
    });
  }

  openDeleteEvaluation(): void {
    const dialogRef = this.dialog.open(DeleteEvaluation, {
      width: '800px',
    });
    dialogRef.afterClosed().subscribe(result => { });
  }
}

@Component({
  selector: 'delete_evaluation',
  templateUrl: 'modals/delete_evaluation.html',
  styleUrls: ['./new-evaluation.component.css']
})
export class DeleteEvaluation {

  evals: any;
  call_id: any;

  constructor(
    public dialogRef: MatDialogRef<DeleteEvaluation>,
    private _dataService: DataService,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) {}

  fetchEvaluation(){
    this._dataService.toDeleteEval(this.call_id).subscribe(data=>{
      this.evals = data;
    })
  }

  deleteEval(id){
    this._dataService.deleteEval(id).subscribe(data=>{
      this.fetchEvaluation();
    })
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}

export class EvaluationForm{
  project: any;
  form: any;
  agent_id: any;
  evaluation_type: any;
  disposition: any;
  call_date: any;
  ref_id: any;
  call_duration: any;
}
