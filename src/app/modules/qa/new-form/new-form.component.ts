import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { MatTableDataSource, MatPaginator } from '@angular/material';
import { DataService } from '../../../services/data/data.service';
import { UsersService } from '../../../services/users/users.service';
import { CommonModelService } from '../../../services/common-model/common-model.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatDialogModule } from '@angular/material';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Subject, Observable, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap, catchError, tap, finalize } from 'rxjs/operators/';
import { AppComponent } from '../../../app.component'; 

@Component({
  selector: 'app-new-form',
  templateUrl: './new-form.component.html',
  styleUrls: ['./new-form.component.css'],
  providers: [MatDialogModule]
})
export class NewFormComponent implements OnInit {
  new_form: Evaluation = new Evaluation();
  admins_list: any =[];
  agents_list: any =[];
  evaluation_form_names: any;
  evaluationmails: any;

  searchTerms = new Subject<string>();
  evaluationForm: FormGroup;
  isLoading = false;
  loading = true;

  form_name_exist: boolean = false;
  form_name_not_exist: boolean = false;
  savingevaluation: boolean = false;
  error: any;
  user_projects: any;
  form_names: any;

  constructor(
    private _dataService: DataService,
    public _userService: UsersService,
    private fb: FormBuilder,
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<NewFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }
  
  ngOnInit(){
    this.evaluationForm = this.fb.group({
      name: null
    })
    this.evaluationForm
    .get('name')
    .valueChanges
    .pipe(
      debounceTime(300),
      tap(() => this.isLoading = true),
      switchMap(value => this._dataService.searchEvaluation(value)
      .pipe(
        finalize(() => this.isLoading = false),
        )))
    .subscribe(evaluations =>{
      this.form_names = evaluations;
      if(this.form_names == true){
        this.form_name_not_exist = true;
        this.form_name_exist = false;
      }else{
        this.form_name_exist = true;
        this.form_name_not_exist = false;
      }
    });
    this.onkeyup();
    this.fetchUserProjects();
  }

  fetchUserProjects(){
    this._userService.getUserProjects(sessionStorage.getItem('user_id')).subscribe(data=>{
      this.user_projects = data;
    });
  }

  onkeyup(){
    this.error = ''
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  addProjectForm(){
    this.new_form['name'] = this.evaluationForm.get('name').value;
    this._dataService.createProjectForm(this.new_form).subscribe(data=>{
      this.dialogRef.close(data);
    });
  }
}

export class Evaluation{
  name: any;
  project_id: any;
}