import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EvaluationReportComponent } from './evaluation-report/evaluation-report.component';
import { NewEvaluationComponent } from './new-evaluation/new-evaluation.component';
import { NewCoachComponent } from './new-coach/new-coach.component';
import { ViewCoachComponent } from './view-coach/view-coach.component';
import { NewCategoryComponent } from './new-category/new-category.component';
import { CategoryQuestionsComponent } from './category-questions/category-questions.component';
import { StartEvaluationComponent } from './start-evaluation/start-evaluation.component';
import { FormsComponent } from './forms/forms.component';

const routes: Routes = [
  {
  	path: 'evaluation-report', 
  	component: EvaluationReportComponent 
  },
  {
  	path: 'new-evaluation', 
  	component: NewEvaluationComponent 
  },
  { 
  	path: 'new-coach', 
  	component: NewCoachComponent 
  },
  { 
    path: 'view-coach', 
    component: ViewCoachComponent 
  },
  { 
    path: 'new-category/:id', 
    component: NewCategoryComponent 
  },
  { 
    path: 'category-questions/:id', 
    component: CategoryQuestionsComponent 
  },
  { 
    path: 'forms', 
    component: FormsComponent 
  },
  { 
    path: 'evaluate', 
    component: StartEvaluationComponent 
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class QaRoutingModule { }
