import { Component, OnInit, ViewChild } from '@angular/core';
import { AppComponent } from '../../../app.component'; 
import { Router, ActivatedRoute } from "@angular/router";
import { DataService } from '../../../services/data/data.service';
import { MatTableDataSource, MatPaginator } from '@angular/material';

@Component({
  selector: 'app-new-category',
  templateUrl: './new-category.component.html',
  styleUrls: ['./new-category.component.css']
})
export class NewCategoryComponent implements OnInit {
  id: any;
  category: Category = new Category();
  create_category: boolean = false;
  details: any;
  form_categories: any;
  dataSource: any;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  displayedColumns: string[] = [ 'name', 'details' ];

  constructor(
  	private router: Router,
    public appcomp: AppComponent,
    public _dataService: DataService,
    private route: ActivatedRoute,
  ) {
    this.appcomp.checkLogin();
  }

  ngOnInit() {
    this.route.params.subscribe(params=>{this.id = params['id']});
    this.fetchFormDetails();
    this.fetchFormCategories();
  }

  openCreateCategoryView(){
    if(this.create_category != true){
      this.create_category = true;
    }
  }
  closeCreateCategoryView(){
    this.create_category = false;
    this.category = new Category();
  }

  fetchFormDetails(){
    this._dataService.getFormDetails(this.id).subscribe(data=>{
      this.details = data;
    });
  }

  createFormCategory(){
  	this.category['form_id'] = this.id;
    this._dataService.createFormCategory(this.category).subscribe(data=>{
    this.router.navigate(['/qa/category-questions/'+data]);
    this.category = new Category();
    });
  }

  fetchFormCategories(){
    this._dataService.getFormCategories(this.id).subscribe(data=>{
      this.form_categories = data;
      this.dataSource = new MatTableDataSource(this.form_categories);
    	this.dataSource.paginator = this.paginator;
    });
  }
}

export class Category{
	name: any;
	form_id: any;
}