import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { AppComponent } from '../../../app.component';
import { DataService } from '../../../services/data/data.service';
import { MatTableDataSource, MatPaginator } from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-category-questions',
  templateUrl: './category-questions.component.html',
  styleUrls: ['./category-questions.component.css']
})
export class CategoryQuestionsComponent implements OnInit {
  form_cat: any;
  question: Question = new Question();
  id: any;
  create_question: boolean = false;
  questions: any;
  index: any;
  dataSource: any;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  displayedColumns: string[] = [
																'question',
																'type',
                                'weight',
                                'answers',
                                'delete',
  														];
  constructor(
    private _dataService: DataService,
    public appcomp: AppComponent,
    private route: ActivatedRoute,
    public dialog: MatDialog,
  ) {
    this.appcomp.reset();
    this.appcomp.checkLogin();
  }

  ngOnInit() {
    this.route.params.subscribe(params=>{this.id = params['id']});
    this.fetchFormCategoryQuestions();
    this.fetchFormCategoryDetails();
  }

  fetchFormCategoryDetails(){
    this._dataService.getFormCategoryDetails(this.id).subscribe(data=>{
      this.form_cat = data;
    });
  }

  fetchFormCategoryQuestions(){
    this._dataService.getFormCategoryQuestions(this.id).subscribe(data=>{
      this.questions = data;
    });
  }

  createQuestion(){
    this.question['form_cat_id'] = this.id;
    this._dataService.createFormCategoryQuestion(this.question).subscribe(data=>{
      this.question = new Question();
      this.create_question = false;
      this.fetchFormCategoryQuestions();
    });
  }

  openCreateQuestionView(){
    if(this.create_question != true){
      this.create_question = true;
    }
  }
  closeCreateQuestionView(){
    this.create_question = false;
    this.question = new Question();
  }

  openEditQuestionDialog(question): void {
    const dialogRef = this.dialog.open(EditQuestionDialog, {
      width: '800px',
      data: {data: question}
    });
    dialogRef.afterClosed().subscribe(result => {
	    this.fetchFormCategoryQuestions();
    });
  }

  openDeleteQuestionDialog(question): void {
    const dialogRef = this.dialog.open(DeleteQuestion, {
      width: '800px',
      data: {data: question}
    });
    dialogRef.afterClosed().subscribe(result => {
	    this.fetchFormCategoryQuestions();
    });
  }
}

export class Question{
	question: any;
	description: any;
	marks: any;
	auto_fail: any;
	text: any;
	weight: any;
}

@Component({
  selector: 'edit-question',
  templateUrl: 'modals/edit-question.html',
})
export class EditQuestionDialog implements OnInit{
  question: Question = new Question();
  constructor(
    private _dataService: DataService,
    public dialogRef: MatDialogRef<EditQuestionDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) {}

  ngOnInit(){
  	this.question = this.data.data;
  	this.question.auto_fail = this.question.auto_fail.toString(); 
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  updateQuestion(){
  	this._dataService.updateQuestion(this.question).subscribe(data=>{
  		this.dialogRef.close();
  	})
  }
}

@Component({
  selector: 'delete-question',
  templateUrl: 'modals/delete-question.html',
})
export class DeleteQuestion implements OnInit{

	question: any;

  constructor(
    public dialogRef: MatDialogRef<DeleteQuestion>,
    private _dataService: DataService,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) {}

  ngOnInit(){
  	this.question = this.data.data;
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  deleteQuestion(){
  	this._dataService.deleteQuestion(this.question.id).subscribe(data=>{
  		this.dialogRef.close();
  	})
  }
}