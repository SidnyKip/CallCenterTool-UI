import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

//routing
import { QaRoutingModule } from './qa-routing.module';

//materials
import { MaterialModule } from '../../material/material.module';

import {
  EvaluationReportComponent,
  ResponseDialog
} from './evaluation-report/evaluation-report.component';
import {
  NewEvaluationComponent,
  DeleteEvaluation,
} from './new-evaluation/new-evaluation.component';
import { NewCoachComponent } from './new-coach/new-coach.component';
import { ViewCoachComponent } from './view-coach/view-coach.component';
import { NewCategoryComponent } from './new-category/new-category.component';
import {
	CategoryQuestionsComponent,
	EditQuestionDialog,
	DeleteQuestion,
} from './category-questions/category-questions.component';
import { StartEvaluationComponent } from './start-evaluation/start-evaluation.component';
import { FormsComponent } from './forms/forms.component';
// import { NewFormComponent } from './new-form/new-form.component';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    QaRoutingModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  declarations: [
  	EvaluationReportComponent,
  	NewEvaluationComponent,
    DeleteEvaluation,
  	NewCoachComponent,
  	ViewCoachComponent,
  	NewCategoryComponent,
  	CategoryQuestionsComponent,
		EditQuestionDialog,
		DeleteQuestion,
		StartEvaluationComponent,
		FormsComponent,
    ResponseDialog,
  	// NewFormComponent,
  ],
  entryComponents: [
		EditQuestionDialog,
		DeleteQuestion,
    ResponseDialog,
    DeleteEvaluation,
	  // NewFormComponent,
  ]
})
export class QaModule { }
