import { Component, OnInit, ViewChild, HostListener } from '@angular/core';
import { DataService } from '../../../services/data/data.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from "@angular/router";
import { MatStepper } from '@angular/material';

@Component({
  selector: 'app-start-evaluation',
  templateUrl: './start-evaluation.component.html',
  styleUrls: ['./start-evaluation.component.css']
})
export class StartEvaluationComponent implements OnInit {

  message:string;
  form: any;
  form_categories: any;
  category: any;
  cat_index: any;
  list_index: any;
  no_next: boolean;
  no_back: boolean;
  questions: any;
  user_score: any;
  firstFormGroup: FormGroup;
  report: Report = new Report();
  update_report: updateReport = new updateReport();
  loading: boolean = false;
  evaluate: boolean = true;
  result: boolean = false;
  @ViewChild('stepper') stepper: MatStepper;
  response_list = [];

  constructor(
    private _dataService: DataService,
    private _formBuilder: FormBuilder,
    private router: Router,
  ) { }

  ngOnInit() {
    this._dataService.currentMessage.subscribe(message => this.message = message);
    if (this.message == 'default message'){
      this.router.navigate(['/qa/new-evaluation']);
    }else{
      this.fetchFormCategories(this.message['form']['id']);
    }
    this.cat_index = 0;
    this.no_back = true;
  }

  pileResponse(c, q, a, marks, move){
    var score;
    a == 'true' ? score = marks : score = 0;
    var obj = {'c':c, 'q':q, 'score':score};
    if(this.response_list.includes(obj)){
      this.list_index = this.response_list.indexOf(obj);
      this.response_list.splice(this.list_index,1);
    }else{
      this.response_list.push(obj)
      this.report = new Report();
    }
    if (move == 'submit'){
      this.submitResponse();
    }
  }

  submitResponse(){
    this.loading = true;
    var res = {
                'evaluation_type':this.message['evaluation_type'],
                'disposition':this.message['disposition'],
                'call_date':this.message['call_date'],
                'ref_id':this.message['ref_id'],
                'call_duration':this.message['call_duration'],
                'project_id':this.message['project']['project_id'],
                'agent_id':this.message['agent_id']['agent_id'],
                'qc': sessionStorage.getItem('user_id'),
                'form_id': this.message['form']['id'],
                'data': this.response_list
              };
    this._dataService.submitResponse(res).subscribe(data=>{
      this.report = new Report();
      this.user_score = data;
      this.loading = false;
      this.evaluate = false;
      this.result = true;
    });
  }

  updateResponse(){
    this.loading = true;
    this.update_report['eval_res_id'] = this.user_score['eval_res_id'];        
    this._dataService.updateResponse(this.update_report).subscribe(data=>{
      this.update_report = new updateReport();
      this.router.navigate(['/qa/new-evaluation']);
    });
  }

  fetchFormCategories(id){
    this._dataService.getFormCategories(id).subscribe(data=>{
      this.form_categories = data;
      this.category = this.form_categories[this.cat_index];
      this.fetchFormCategoryQuestions(this.category.id);
    });
  }

  fetchFormCategoryQuestions(id){
    this.questions = [];
    this._dataService.getFormCategoryQuestions(id).subscribe(res=>{
      this.questions = res;
    });
  }

  nextTab(action){
      action == 'next' ? this.cat_index= this.cat_index+1 : this.cat_index=this.cat_index-1 ;
      this.category = this.form_categories[this.cat_index];
      this.cat_index == 0 ? this.no_back = true : this.no_back = false;
      this.cat_index+1 == this.form_categories.length ? this.no_next = true : this.no_next = false;
      this.fetchFormCategoryQuestions(this.form_categories[this.cat_index].id);
      this.stepper.selectedIndex = 0;
  }
}

export class Report { }
export class updateReport { }
