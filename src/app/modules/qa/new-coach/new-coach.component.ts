import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { MatTableDataSource, MatPaginator } from '@angular/material';
import { DataService } from '../../../services/data/data.service';
import { UsersService } from '../../../services/users/users.service';
import { CommonModelService } from '../../../services/common-model/common-model.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatDialogModule } from '@angular/material';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Subject, Observable, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap, catchError, tap, finalize } from 'rxjs/operators/';
import { AppComponent } from '../../../app.component'; 
import { Router } from "@angular/router";
import { DatePipe } from '@angular/common';


@Component({
  selector: 'app-new-coach',
  templateUrl: './new-coach.component.html',
  styleUrls: ['./new-coach.component.css']
})
export class NewCoachComponent implements OnInit {

  user_projects: any;
  forms_list: any = [];
  
  constructor(
    public _dataService: DataService,
    public _userService: UsersService,
    private commModel: CommonModelService,
    private router: Router,
    public dialog: MatDialog,
    public appcomp: AppComponent,
    public datepipe: DatePipe,
  ) {
    this.appcomp.checkLogin();
  }

  ngOnInit() {
    this.fetchUserProjects();
  }
  fetchUserProjects(){
    this._userService.getUserProjects(sessionStorage.getItem('user_id')).subscribe(data=>{
      this.user_projects = data;
    });
  }

  fetchProjectForms(project_id){
    this.forms_list = [];
    this._dataService.getProjectForms(project_id, sessionStorage.getItem('user_id')).subscribe(data=>{
      this.forms_list = data;
      // Only show forms that have categories
      this.forms_list.forms = this.forms_list.forms.filter(form => form.total > 0);
    });
  }

}
