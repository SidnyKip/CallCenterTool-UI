import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { MatTableDataSource, MatPaginator } from '@angular/material';
import { UsersService } from '../../../services/users/users.service';
import { DataService } from '../../../services/data/data.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Subject, Observable, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap, catchError, tap, finalize } from 'rxjs/operators/';
import { AppComponent } from '../../../app.component'; 
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-evaluation-report',
  templateUrl: './evaluation-report.component.html',
  styleUrls: ['./evaluation-report.component.css']
})
export class EvaluationReportComponent implements OnInit {

  countries: any;
  projects: any;
  forms_list: any;
  role: any;
  isSuperAdmin: boolean;
  isNormalAdmin: boolean;
  isQc: boolean = false;
  loading_filter: boolean = false;
  load_download: boolean = false;
  all_view: boolean = false;
  agent_view: boolean = false;
  agent_info: any;
  rpt_team: boolean = false;
  rpt_project: boolean = false;
  rpt_agents: boolean = false;

  report: Report = new Report();
  esc_report: any;
  myDate = new Date();

  constructor(
    public _userService: UsersService,
    public dialog: MatDialog,
    public appcomp: AppComponent,
    public _dataService: DataService,
    public datepipe: DatePipe,
  ) {
    this.appcomp.checkLogin();
  }

  ngOnInit() {
    console.log(this.myDate)
  	this.role = sessionStorage.getItem('role');
  	if(this.role == 1){
  		this.isSuperAdmin = true;
  	}else{
  		this.isNormalAdmin = true;
	    this.fetchUserProjects();
  	}
    this.fetchCountries();
    
    if(sessionStorage.getItem('user_type') == '2'){
      this.report['qc_id'] = sessionStorage.getItem('user_id');
      this.isQc = true;
    }else if(sessionStorage.getItem('user_type') == '1'){
      this.report['agent_id'] = sessionStorage.getItem('user_id');
    }
  }

  exportTableToCSV() {
    var filename = 'Evaluation Report '+this.datepipe.transform(this.myDate, 'yyyy-MM-dd').toString()+'.csv';
    var csv = [];
    var rows = <any> document.querySelectorAll("tr");
    for (var i = 0; i < rows.length; i++) {
      var row = [];
      var cols = rows[i].querySelectorAll("td");
      for (var j = 0; j < cols.length; j++)
        if(cols[j].innerText != '#' && cols[j].innerText != 'VIEW'){
          row.push(cols[j].innerText);
        }
        // row.push(cols[j].innerText);
      csv.push(row.join(","));
    }
    // Download CSV file
    this.downloadCSV(csv.join("\n"), filename);
  }

  downloadCSV(csv, filename) {
    var csvFile;
    var downloadLink;
    // CSV file
    csvFile = new Blob([csv], {type: "text/csv"});
    // Download link
    downloadLink = document.createElement("a");
    // File name
    downloadLink.download = filename;
    // Create a link to the file
    downloadLink.href = window.URL.createObjectURL(csvFile);
    // Hide download link
    downloadLink.style.display = "none";
    // Add the link to DOM
    document.body.appendChild(downloadLink);
    // Click download link
    downloadLink.click();
  }

  fetchUserProjects(){
    this.projects = [];
    this._userService.getUserProjects(sessionStorage.getItem('user_id')).subscribe(data=>{
      this.projects = data;
      for(let x of this.projects){
      	x['id'] = x['project_id']
      }
    });
  }

  viewAgentReport(item){
    this.agent_view = true;
    this.all_view = false;
    this.agent_info = item;
  }
  
  goBack(){
    this.agent_view = false;
    this.all_view = true;
  }

  fetchCountries(){
    this._userService.getCountries().subscribe(data=>{
      this.countries = data;
    })
  }

  fetchCountryProjects(country_id){
    this.projects = [];
    this._userService.getCountryProject(country_id).subscribe(data=>{
      this.projects = data;
    })
  }

  fetchProjectForms(project_id){
    this.forms_list = [];
    this._dataService.getProjectForms(project_id, 0).subscribe(data=>{
      this.forms_list = data;
      this.forms_list = this.forms_list.forms;
    });
  }

  fetchEvaluationReport(){
    this.esc_report = [];
    this.loading_filter = true;
    this.report['datefrom'] = this.datepipe.transform(this.report['datefrom'], 'yyyy-MM-dd');
    this.report['dateto'] = this.datepipe.transform(this.report['dateto'], 'yyyy-MM-dd');
    this._dataService.getEvaluationReport(this.report).subscribe(data=>{
      this.all_view = true;
      this.agent_view = false;
      this.loading_filter = false;
      this.esc_report = data;
      if (this.report['type'] == 'project') {
        this.rpt_project = true;
        this.rpt_team = false;
        this.rpt_agents = false;
      }else if (this.report['type'] == 'agent') {
        this.rpt_agents = true;
        this.rpt_team = false;
        this.rpt_project = false;
      }else{
        this.rpt_team = true;
        this.rpt_project = false;
        this.rpt_agents = false;
      }
    });
  }

  // downloadEvaluationReport(){
  //   this.load_download = true;
  //   this.report['datefrom'] = this.datepipe.transform(this.report['datefrom'], 'yyyy-MM-dd');
  //   this.report['dateto'] = this.datepipe.transform(this.report['dateto'], 'yyyy-MM-dd');
  //   this._dataService.downloadEvaluationReport(this.report).subscribe(data=>{
  //     this.agent_reportdata = data;
  //     window.location.replace(this.agent_reportdata.url)
  //     this.load_download = false;
  //   }, error =>{
  //       this.agent_reportdata = error;
  //       window.location.replace(this.agent_reportdata.url)
  //     this.load_download = false;
  //   });
  // }

  openResponseDialog(agent_names, eval_res_id) {
    const dialogRef = this.dialog.open(ResponseDialog, {
      width: '800px',
      data: {'agent_names': agent_names, 'eval_res_id': eval_res_id}
    });
    dialogRef.afterClosed().subscribe(result => {
      // this.fetchFormCategoryQuestions();
    });
  }
}

export class Report{
  country: any;
  project: any;
  form: any;
  type: any;
  datefrom: any;
  dateto: any;
}

@Component({
  selector: 'response',
  templateUrl: 'modals/response.html',
  styleUrls: ['./evaluation-report.component.css']
})
export class ResponseDialog implements OnInit{

  agent_names: any;
  eval_res_id: any;
  response: any;

  constructor(
    public dialogRef: MatDialogRef<ResponseDialog>,
    private _dataService: DataService,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) {}

  ngOnInit(){
    this.agent_names = this.data.agent_names;
    this.eval_res_id = this.data.eval_res_id;
    this._dataService.getEvaluationResponse(this.eval_res_id).subscribe(data=>{
      this.response = data;
    })
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  fetchResponse(){
  }
}