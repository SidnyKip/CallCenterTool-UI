import { Component, OnInit, ViewChild } from '@angular/core';
import { AppComponent } from '../../../app.component'; 
import { Router, ActivatedRoute } from "@angular/router";
import { DataService } from '../../../services/data/data.service';
import { MatTableDataSource, MatPaginator } from '@angular/material';
import { CommonModelService } from '../../../services/common-model/common-model.service';
import { UsersService } from '../../../services/users/users.service';


@Component({
  selector: 'app-forms',
  templateUrl: './forms.component.html',
  styleUrls: ['./forms.component.css']
})
export class FormsComponent implements OnInit {
  id: any;
  category: Category = new Category();
  create_category: boolean = false;
  details: any;
  form_categories: any;
  dataSource: any;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  displayedColumns: string[] = [ 'name', 'details' ];
	forms_list: any;
	forms_list_length: any;
	user_projects: any;

  constructor(
  	private router: Router,
    public appcomp: AppComponent,
    public _dataService: DataService,
    private route: ActivatedRoute,
    private commModel: CommonModelService,
    public _userService: UsersService,
  ) {
    this.appcomp.checkLogin();
  }

  ngOnInit() {
    this.route.params.subscribe(params=>{this.id = params['id']});
    this.fetchUserProjects();
  }

  createFormCategory(){
  	this.category['form_id'] = this.id;
    this._dataService.createFormCategory(this.category).subscribe(data=>{
    this.router.navigate(['/qa/category-questions/'+data]);
    this.category = new Category();
    });
  }

  fetchUserProjects(){
    this._userService.getUserProjects(sessionStorage.getItem('user_id')).subscribe(data=>{
      this.user_projects = data;
    });
  }

  fetchProjectForms(project_id){
    this.forms_list = [];
    this._dataService.getProjectForms(project_id, 0).subscribe(data=>{
      this.forms_list = data;
      this.dataSource = new MatTableDataSource(this.forms_list.forms);
      this.forms_list_length = this.forms_list.forms.length;
      this.dataSource.paginator = this.paginator;
    });
  }

  openNewFormDialog(): void {
    this.commModel.openDialog().subscribe(data =>{
      if (data != undefined){
        this.router.navigate(['/qa/new-category/'+data]);
      }
    });
  }
}

export class Category{
	name: any;
	form_id: any;
}