import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

//routing
import { AdminRoutingModule } from './admin-routing.module';

//materials
import { MaterialModule } from '../../material/material.module';

import { FileUploadModule } from 'ng2-file-upload';
import { TimeAgoPipe } from 'time-ago-pipe';

// components
import {
  DashboardComponent,
  UpdateViewsDialog,
  EditUpdateDialog,
} from './dashboard/dashboard.component';
import { 
  HuddleComponent,
  HuddleDetailDialog,
} from './huddle/huddle.component';
import { ClarifyComponent } from './clarify/clarify.component';
import { ScrollToBottomDirective } from './clarify/scroll-to-bottom';
import { AllComponent } from './escalations/all/all.component';
import { ReportComponent } from './escalations/report/report.component';
import { NewComponent } from './escalations/new/new.component';

@NgModule({
  imports: [
    CommonModule,
    AdminRoutingModule,
    FileUploadModule,
  	FormsModule,
  	ReactiveFormsModule,
    MaterialModule,
  ],
  declarations: [
  	DashboardComponent,
    HuddleComponent,
    HuddleDetailDialog,
    UpdateViewsDialog,
    EditUpdateDialog,
    ClarifyComponent,
    ScrollToBottomDirective,
    TimeAgoPipe,
    AllComponent,
    ReportComponent,
    NewComponent,
  ],
  entryComponents:[
    HuddleDetailDialog,
    UpdateViewsDialog,
    EditUpdateDialog,
  ]
})
export class AdminModule { }
